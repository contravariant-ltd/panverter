Depth=   81751 States=    1e+06 Transitions= 3.03e+06 Memory=   763.523	t=      3.4 R=   3e+05

(Spin Version 6.5.2 -- 6 December 2019)
	+ Partial Order Reduction

Full statespace search for:
	never claim         	- (none specified)
	assertion violations	+
	acceptance   cycles 	- (not selected)
	invalid end states	+

State-vector 104 byte, depth reached 81751, errors: 0
  1700071 states, stored
  3117605 states, matched
  4817676 transitions (= stored+matched)
 14461253 atomic steps
hash conflicts:     84355 (resolved)

Stats on memory usage (in Megabytes):
  214.013	equivalent memory usage for states (stored*(State-vector + overhead))
  168.814	actual memory usage for states (compression: 78.88%)
         	state-vector as stored = 76 byte + 28 byte overhead
  128.000	memory used for hash table (-w24)
  534.058	memory used for DFS stack (-m10000000)
    1.902	other (proc and chan stacks)
  832.956	total actual memory usage


unreached in proctype Engine
	./Engine.pml:47, state 19, "D_STEP47"
	./Engine.pml:47, state 19, "messages[((((i*2)+j)*3)+k)] = 126"
	./Engine.pml:44, state 27, "j = 0"
	Model.pml:162, state 96, "-end-"
	(3 of 96 states)
unreached in proctype Terminal
	./Terminal.pml:242, state 174, "-end-"
	(1 of 174 states)
unreached in proctype LoadGenerator
	(0 of 39 states)
unreached in init
	(0 of 45 states)

pan: elapsed time 5.45 seconds
pan: rate 311939.63 states/second
	Command being timed: "./pan -m10000000"
	User time (seconds): 5.31
	System time (seconds): 0.35
	Percent of CPU this job got: 99%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:05.66
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 853640
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 107147
	Voluntary context switches: 8
	Involuntary context switches: 19
	Swaps: 0
	File system inputs: 0
	File system outputs: 8
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
